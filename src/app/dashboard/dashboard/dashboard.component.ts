import { Component, OnInit } from '@angular/core';
import { KoineService } from 'src/app/services/koine.service';

@Component({
  selector: 'koine-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  assetData = [];
  selectedAccount = {};

  constructor(private api: KoineService) { }

  ngOnInit() {
    // call Koine service here to get accounts
    // look at Angular Observables and Subscription docs
  }

  setSelected(e) {
    this.selectedAccount = e;
    this.assetData.map(el => {
        el.selected = false;
        if (el.id === e.id) {
          el.selected = true;
        }
    });
  }

}
