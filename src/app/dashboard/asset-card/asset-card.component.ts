import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'koine-asset-card',
  templateUrl: './asset-card.component.html',
  styleUrls: ['./asset-card.component.scss']
})
export class AssetCardComponent implements OnInit {

  @Input() data: any;
  @Output() selectCard: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onSelect() {
    this.selectCard.emit(this.data);
  }

}
