import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CoinbaseService {

  // https://docs.pro.coinbase.com/#get-historic-rates
  // examplePath = `/products/${symbol}-USD/candles?start=${start_date}&end=${end_date}`;

  constructor(private http: HttpClient) { }
}
